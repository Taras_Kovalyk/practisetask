﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace Tasks
{
    public class Rectangle
    {
       public struct Point
       {
            public int X;
            public int Y;

            public override string ToString() => $"{{ { X }, { Y } }}";           

            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }
        }

        private Point _bottomLeft;
        private Point _bottomRight;
        private Point _topLeft;
        private Point _topRight;

        private int _width;
        private int _height;

        public int Width      => _width;
    
        public int Height     => _height;
    
        public Point BottomLeft  => _bottomLeft;

        public Point BottomRight => _bottomRight;

        public Point TopLeft     => _topLeft;

        public Point TopRight    => _topRight;
  

        public void ChangeLocationByXY(int shiftX, int shiftY)
        {        
            _topLeft.X += shiftX;    
            _topRight.X += shiftX;     
            _bottomLeft.X += shiftX;     
            _bottomRight.X += shiftX;   

            _topLeft.Y += shiftY;    
            _topRight.Y += shiftY;     
            _bottomLeft.Y += shiftY;     
            _bottomRight.Y += shiftY;   
        }

         public void ShiftByX(int shiftX)
        {        
            _topLeft.X += shiftX;    
            _topRight.X += shiftX;     
            _bottomLeft.X += shiftX;     
            _bottomRight.X += shiftX;               
        }

          public void ShiftByY(int shiftY)
        {        
            _topLeft.Y += shiftY;    
            _topRight.Y += shiftY;     
            _bottomLeft.Y += shiftY;     
            _bottomRight.Y += shiftY;          
        }

        public void SetNewSize(int newWidth, int newHeight)
        {            
            if(newWidth  == 0) throw new ArgumentException($"{ nameof(newWidth)  } cannot be equal 0");            
            if(newHeight == 0) throw new ArgumentException($"{ nameof(newHeight) } cannot be equal 0");

            _bottomRight = new Point(_bottomLeft.X + newWidth, _bottomLeft.Y);
            _topLeft     = new Point(_bottomLeft.X, _bottomLeft.Y + newHeight);
            _topRight    = new Point(_bottomLeft.X + newWidth, _bottomLeft.Y + newHeight);
           
            _width = newWidth;
            _height = newHeight;
        }
        
         public void ChangeWidth(int deltaWidth)
        {           
             _width += deltaWidth;    

            if(_width == 0) throw new ArgumentException($"{ nameof(_width) } cannot be equal 0");   
            
            _bottomRight = new Point(_bottomLeft.X + _width, _bottomLeft.Y);         
            _topRight    = new Point(_bottomLeft.X + _width, _topRight.Y);           
        }

          public void ChangeHeight(int deltaHeight)
        {           
            _height += deltaHeight;

            if(_height == 0) throw new ArgumentException($"{ nameof(_height) } cannot be equal 0");            
            
            _topLeft  = new Point(_bottomLeft.X, _bottomLeft.Y + _height);
            _topRight = new Point(_bottomRight.X, _bottomRight.Y + _height);          
        }         

        private void OrderPointsByLocation(Point pointA, Point pointB, Point pointC, Point pointD)
        {
            var xCoords = new List<int>(){ pointA.X, pointB.X, pointC.X, pointD.X }.Distinct().ToList<int>();
            var yCoords = new List<int>(){ pointA.Y, pointB.Y, pointC.Y, pointD.Y }.Distinct().ToList<int>();

            _bottomLeft  = new Point(xCoords.Min(), yCoords.Min());
            _bottomRight = new Point(xCoords.Max(), yCoords.Min());
            _topLeft     = new Point(xCoords.Min(), yCoords.Max());
            _topRight    = new Point(xCoords.Max(), yCoords.Max());
        }
       /* public void ContainsTwo(Rectangle rec1, Rectangle rec2)
        {
            int thirdRectangleWidth, thirdRectangleHeight;
            int firstLength = (rec1._bottomLeft.X + rec1._width) + (rec1._bottomLeft.Y + rec1._height);
            int secondLength = (rec2._bottomLeft.X + rec2._width) + (rec2._bottomLeft.Y + rec2._height);
            if (firstLength > secondLength)
            {
                thirdRectangleWidth = rec1._bottomLeft.X + rec1._width;
                thirdRectangleHeight = rec1._bottomLeft.Y + rec1._height;
            }
            else
            {
                thirdRectangleWidth = rec2._bottomLeft.X + rec2._width;
                thirdRectangleHeight = rec2._bottomLeft.Y + rec2._height;
            }
            Console.WriteLine($"Width: {thirdRectangleWidth}, Height:{thirdRectangleHeight}");
        }*/

        public Rectangle InterSect(Rectangle rectangle)
        {
            int totalRectanglesWidth  = rectangle._width + _width;
            int totalRectanglesHeight = rectangle._height + _height;
            
            int minY = Math.Min(_bottomLeft.Y, rectangle._bottomLeft.Y);
            int maxY = Math.Max(_topLeft.Y, rectangle._topLeft.Y);
            int minX = Math.Min(_bottomLeft.X, rectangle._bottomLeft.X);
            int maxX = Math.Max(_bottomRight.X, rectangle._bottomRight.X);             

            if(Math.Abs(maxY - minY) >= totalRectanglesHeight || Math.Abs(maxX - minX) >= totalRectanglesWidth)
            {   
                Console.WriteLine("There is no intersection for given rectangles");
                return null;
            }
            else
            {
                 int x1, x2, y1, y2;                        
             
                x1 = (_topLeft.X != rectangle._topLeft.X) ? ((minX == _topLeft.X) ? (minX + _width) : 
                     (minX + rectangle._width)) : (minX + Math.Min(_width, rectangle._width));

                y1 = (_bottomLeft.Y != rectangle._bottomLeft.Y) ? ((minY == _bottomLeft.Y) ? (minY + _height) :
                     (minY + rectangle._height)) : (minY + Math.Min(_height, rectangle._height));

                x2 = (_topRight.X != rectangle._topRight.X) ? ((maxX == _topRight.X) ? (maxX - _width) :
                     (maxX - rectangle._width)) : (maxX - Math.Min(_width, rectangle._width));

                y2 = (_topRight.Y != rectangle._topRight.Y) ? ((maxY == _topLeft.Y) ? (maxY - _height) :
                     (maxY - rectangle._height)) : (maxY - Math.Min(_height, rectangle._height));

                   var bottomLeft = new Point(x2, y2);
                   var bottomRight = new Point(x1, y2);
                   var topLeft = new Point(x2, y1);
                   var topRight = new Point(x1, y1);

                bool isLeftRectangleNested = ((_topLeft.Y <= rectangle._topLeft.Y) && (_bottomLeft.Y >= rectangle._bottomLeft.Y) && 
                                (_topLeft.X >= rectangle._topLeft.X) && (_bottomRight.X <= rectangle._bottomRight.X));
                bool isRightRectangleNested = ((rectangle._topLeft.Y <= _topLeft.Y) && (rectangle._bottomLeft.Y >= _bottomLeft.Y) && 
                                (rectangle._topLeft.X >= _topLeft.X) && (rectangle._bottomRight.X <= _bottomRight.X));          

                if (isLeftRectangleNested)  return new Rectangle(_bottomLeft, _bottomRight, _topLeft, _topRight);
                if (isRightRectangleNested) return rectangle;                              

                return new Rectangle(bottomLeft, bottomRight, topLeft, topRight);
            }            
        }

        private bool ValidateInputPoints(Point pointA, Point pointB, Point pointC, Point pointD)
        {
           var points = new List<Point>(){ pointA, pointB, pointC, pointD };
           var validationCounter = 0;

           bool xEqual_yUnequal,
                yEqual_xUnequal;

            for(var i = 0; i < points.Count; i++)
            {
                for(var j = 0; j < points.Count; j++)
                {
                    xEqual_yUnequal = (points[i].X == points[j].X) && (points[i].Y != points[j].Y);
                    yEqual_xUnequal = (points[i].X != points[j].X) && (points[i].Y == points[j].Y);

                    if((i != j) && (xEqual_yUnequal || yEqual_xUnequal))
                    {
                        validationCounter++;
                    }

                    if((i != j) && (points[i].X == points[j].X) && (points[i].Y == points[j].Y))
                    {
                        return false;
                    }
                }
            }  

            return (validationCounter == 8) ? true : false;
        }

        public Rectangle(int bottomLeftX, int bottomLeftY, int width, int height)
        {
            if(width  == 0) throw new ArgumentException($"{ nameof(width)  } cannot be equal 0");            
            if(height == 0) throw new ArgumentException($"{ nameof(height) } cannot be equal 0");

            _bottomLeft  = new Point(bottomLeftX, bottomLeftY);
            _bottomRight = new Point(bottomLeftX + width, bottomLeftY);
            _topLeft     = new Point(bottomLeftX, bottomLeftY + height);
            _topRight    = new Point(bottomLeftX + width, bottomLeftY + height);

            _width = width;
            _height = height;    
        }

         public Rectangle(Point pointA, Point pointB, Point pointC, Point pointD)
        {
             if(!ValidateInputPoints(pointA, pointB, pointC, pointD))
            {
               throw new ArgumentException("Cannot create rectangle based on these points");
            }

            OrderPointsByLocation(pointA, pointB, pointC, pointD);         

            _width  = Math.Abs(_topLeft.X - _topRight.X);
            _height = Math.Abs(_bottomLeft.Y - _topLeft.Y);          
       
        }
    }
}
