﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks
{
     public class Vector:IEnumerable
    {
        private int[] vectorData;

        public int Length => vectorData.Length;

        public int this[int index]
        {
            get
            {
                if(index < 0 || index >= Length)
                {                    
                    throw new ArgumentOutOfRangeException($"{ nameof(index) } mustn't be negative or more than current vector length.");                                 
                }
                return vectorData[index];
            }
            set
            {
                if (index < 0 || index >= Length)
                {
                    throw new ArgumentOutOfRangeException($"{ nameof(index) } mustn't be negative or more than current vector length.");
                }
                vectorData[index] = value;
            }
        }

        public override string ToString()
        {
            string result = "";
            foreach(var item in vectorData)
           {
               result += item.ToString() + " ";
           }
            return result;
        }

        public static Vector Add(Vector vectorA, Vector vectorB)
        {            
            if (vectorA.Length != vectorB.Length)
            {               
                throw new ArgumentException("Input vectors have not equal lengths.");             
            }

            var resultVector = new Vector(vectorA.Length);
                
           for(var i = 0; i < vectorA.Length; i++)
            {
                resultVector[i] = checked(vectorA[i] + vectorB[i]);
            }

            return resultVector;
        }        

        public static Vector Subtract(Vector vectorA, Vector vectorB)
        {
            if (vectorA.Length != vectorB.Length)
            {                
                throw new ArgumentException("Input vectors have not equal lengths.");               
            }

            var resultVector = new Vector(vectorA.Length);

            for (var i = 0; i < vectorA.Length; i++)
            {
                resultVector[i] = checked(vectorA[i] - vectorB[i]);
            }

            return resultVector;
        }

           public static Vector MultiplyByScalar(Vector vector, int scalar)
        { 
            var resultVector = new Vector(vector.Length);

              for (var i = 0; i < vector.Length; i++)
              {
                  resultVector[i] = checked(vector[i] * scalar);
              }            
              return resultVector;
        }


        public static Vector operator+ (Vector vectorA, Vector vectorB) => Vector.Add(vectorA, vectorB);

        public static Vector operator- (Vector vectorA, Vector vectorB) => Vector.Subtract(vectorA, vectorB);
        
        public static Vector operator* (Vector vector, int scalar) => Vector.MultiplyByScalar(vector, scalar);        

        public static Vector operator* (int scalar, Vector vector) => Vector.MultiplyByScalar(vector, scalar);

        public static Vector operator- (Vector vector)
        {
            for(var i = 0; i < vector.Length; i++)
            {
                vector[i] *= -1;
            }
            return vector;
        }        
             
        public static bool operator== (Vector vectorA, Vector vectorB)
        {
             if (vectorA.Length != vectorB.Length)
            {
                return false;
            }

            bool equal = true;

            for (var i = 0; i < vectorB.Length; i++)
            {
                if (vectorA[i] != vectorB[i])
                {
                    equal = false;
                    break;
                }
            }
            return equal;
        }

        public static bool operator!= (Vector vectorA, Vector vectorB)
        {
             if (vectorA.Length != vectorB.Length)
            {
                return true;
            }

            bool nonEqual = false;
            
            for (var i = 0; i < vectorB.Length; i++)
            {
                if (vectorA[i] != vectorB[i])
                {
                   nonEqual = true;
                   break;
                }               
            }

            return nonEqual;
        }

        public override bool Equals(object vector) => Equals((Vector)vector);   

        public bool Equals(Vector vector)
        {
            if (vectorData.Length != vector.Length)
            {
                return false;
            }

            bool equal = true;

            for (var i = 0; i < vector.Length; i++)
            {
                if (vector[i] != vectorData[i])
                {
                    equal = false;
                    break;
                }
            }
            return equal;
        }       

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = 23;
                foreach (var item in vectorData)
                {
                    hashCode = hashCode * 31 + item.GetHashCode();
                }
                return hashCode;
            }          
        }

        public IEnumerator GetEnumerator()
        {
            foreach(var item in vectorData)
            {
                yield return item;
            }
        }
            
        public Vector(int vectorLength)
        {
            if(vectorLength <= 0)
            {               
                throw new ArgumentOutOfRangeException($"{ nameof(vectorLength) } of vector must be more than zero.");                         
            }
            vectorData = new int[vectorLength];
        }

        public Vector(int[] vectorElements)
        {           
            if (vectorElements.Length == 0)
            {              
                throw new ArgumentOutOfRangeException($"Length of { nameof(vectorElements) } must be more than zero.");                          
            }
            vectorData = vectorElements; 
        }
    }
}
