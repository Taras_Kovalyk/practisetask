﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks
{
    public class Matrix
    {
       private int[,] _matrix;

       private int _rowCount;
       private int _columnCount;

        public int RowCount => _rowCount;
        public int ColumnCount => _columnCount;

        public int this[int rowIndex, int columnIndex]
       {
            get
            {
                if( (rowIndex < 0) || (rowIndex >= _rowCount) || (columnIndex < 0) || (columnIndex >= _columnCount))
                {
                    throw new ArgumentOutOfRangeException($"{ nameof(rowIndex) } and { nameof(columnIndex) } mustn't be negative or more than { nameof(_rowCount) } and { nameof(_rowCount) } appropriately.");
                }              
                 return _matrix[rowIndex, columnIndex];
            }
            set
            {
                if ((rowIndex < 0) || (rowIndex >= _rowCount) || (columnIndex < 0) || (columnIndex >= _columnCount))
                {
                    throw new ArgumentOutOfRangeException($"{ nameof(rowIndex) } and { nameof(columnIndex) } mustn't be negative or more than { nameof(_rowCount) } and { nameof(_rowCount) } appropriately.");   
                }
                _matrix[rowIndex, columnIndex] = value;
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder(_matrix.Length * 2 + _matrix.GetLength(0));

            for(var i = 0; i < _matrix.GetLength(0); i++)
            {
                for(var j = 0; j < _matrix.GetLength(1); j++)
                {
                    result.Append(_matrix[i, j].ToString() + " ");
                }
                result.Append("\n");
            }
            return result.ToString();
        }

        public static Matrix Add(Matrix matrixA, Matrix matrixB)
        {
            if((matrixA._rowCount != matrixB._rowCount) || (matrixB._columnCount != matrixA._columnCount))
            {
               throw new ArgumentException($"Size of { nameof(matrixA) } doesn't equal to size of { nameof(matrixB) }.");                  
            }

            Matrix matrixC = new Matrix(matrixA._rowCount, matrixA._columnCount);

            for(var i = 0; i < matrixA._rowCount; i++)
            {
               for(var j = 0; j < matrixA._columnCount; j++)
               {
                    matrixC[i, j] = matrixA[i, j] + matrixB[i, j]; 
               }
            }
            return matrixC;
        }

        public static Matrix Subtract(Matrix matrixA, Matrix matrixB)
        {
            if((matrixA._rowCount != matrixB._rowCount) || (matrixB._columnCount != matrixA._columnCount))
            {
               throw new ArgumentException($"Size of { nameof(matrixA) } doesn't equal to size of { nameof(matrixB) }.");                  
            }

            Matrix matrixC = new Matrix(matrixA._rowCount, matrixA._columnCount);

            for(var i = 0; i < matrixA._rowCount; i++)
            {
               for(var j = 0; j < matrixA._columnCount; j++)
               {
                    matrixC[i, j] = matrixA[i, j] - matrixB[i, j]; 
               }
            }
              return matrixC;
        }

        public static Matrix Multiply(Matrix matrixA, Matrix matrixB)
        {
            if(matrixA._columnCount != matrixB._rowCount)
            {
                throw new ArgumentException($"Columns count of { nameof(matrixA) } must be equal to rows count of { nameof(matrixB) }");
            }

            Matrix matrixC = new Matrix(new int[matrixA._rowCount, matrixB._columnCount]);

            for(var i = 0; i < matrixA._rowCount; i++)
            {
            //    for(var i = 0; i < matrixB._columnCount; j++)
            }


            return matrixC; 

        }

        public static Matrix operator+ (Matrix matrixA, Matrix matrixB) => Add(matrixA, matrixB);
        
        public static Matrix operator- (Matrix matrixA, Matrix matrixB) => Subtract(matrixA, matrixB);
        
        public static Matrix operator- (Matrix matrix)
        {
            for(var i = 0; i < matrix._rowCount; i++)
            {
               for(var j = 0; j < matrix._columnCount; j++)
               {
                    matrix[i, j] *= -1;
               }
            }
            return matrix;
        }            

        public Matrix(int[,] matrix)
        {         
            _rowCount = matrix.GetLength(0);
            _columnCount = matrix.GetLength(1);

            if ((_rowCount == 0) || (_columnCount == 0))
            {
                throw new ArgumentOutOfRangeException($"Rows and Columns count must be more than 0.");
            }

            _matrix = matrix;
           
        }

        public Matrix(int rowCount, int columnCount)
       {
            if ((rowCount <= 0) || (columnCount <= 0))
            {
                throw new ArgumentOutOfRangeException($"{ nameof(rowCount) } and { nameof(columnCount) } mustn't be negative or equal 0.");
            }

            _rowCount = rowCount;
            _columnCount = columnCount;

            _matrix = new int[_rowCount, _columnCount];
        }


    }
}
